<?php
   
    add_action( 'cmb2_admin_init', 'suspensao_metaboxes' );

    function suspensao_metaboxes() {

            $prefix = 'AASP_';
            
            $cmb = new_cmb2_box( array(
                    'id'              => 'suspensao',
                    'title'           => __( 'Sistema de suspensão', 'cmb2' ),
                    'object_types'    => array( 'suspensao'), // Post type
                    'context'         => 'normal',
                    'priority'        => 'high',
                    'show_names'      => true, // Show field names on the left
                    // 'cmb_styles' => false, // false to disable the CMB stylesheet
                    // 'closed'     => true, // Keep the metabox closed by default
            ) );

            /*$cmb->add_field( array(
                    'name'            => 'Descrição do CTP',
                    'desc'            => 'This is a title description',
                    'type'            => 'title',
                    'id'              => 'wiki_test_title'
            ) );*/

            $cmb->add_field( array(
                    'name'            => __( 'Selecione o estado' ),
                    'desc'            => 'Estado de origem. (opcional)',
                    'id'              => $prefix . 'estado_endereco',
                    'type'            => 'select',
                    'show_option_none'=> true,
                    'options'         => array(
                    'SP'              => __( 'São Paulo' ),
                    'AC'              => __( 'Acre' ),
                    'AL'              => __( 'Alagoas' ),
                    'AP'              => __( 'Amapá' ),
                    'AM'              => __( 'Amazonas' ),
                    'BA'              => __( 'Bahia' ),
                    'CE'              => __( 'Ceará' ),
                    'DF'              => __( 'Distrito Federal' ),
                    'ES'              => __( 'Espírito Santo' ),
                    'GO'              => __( 'Goiás' ),
                    'MA'              => __( 'Maranhão' ),
                    'MT'              => __( 'Mato Grosso' ),
                    'MS'              => __( 'Mato Grosso do Sul' ),
                    'MG'              => __( 'Minas Gerais' ),
                    'PA'              => __( 'Pará' ),
                    'PB'              => __( 'Paraíba' ),
                    'PR'              => __( 'Paraná' ),
                    'PE'              => __( 'Pernambuco' ),
                    'PI'              => __( 'Piauí' ),
                    'RR'              => __( 'Roraima' ),
                    'RO'              => __( 'Rondônia' ),
                    'RJ'              => __( 'Rio de Janeiro' ),
                    'RN'              => __( 'Rio Grande do Norte' ),
                    'RS'              => __( 'Rio Grande do Sul' ),
                    'SC'              => __( 'Santa Catarina' ),
                    'SE'              => __( 'Sergipe' ),
                    'TO'              => __( 'Tocantins' ),
                    ),
            ) );

            $cmb->add_field( array(
                    'name'            => __( 'Cidade', 'cmb2' ),
                    'desc'            => __( 'Cidade de origem. (opcional)', 'cmb2' ),
                    'id'              => $prefix  .'cidade',
                    'type'            => 'text',
                    'show_on_cb'      => 'cmb2_hide_if_no_cats', // function should return a bool value
            ) );

            /*$cmb->add_field( array(
                    'name'            => __( 'Tribunal / Fórum', 'cmb2' ),
                    'desc'            => __( 'Tribunal / Fórum (opcional)', 'cmb2' ),
                    'id'              => $prefix . 'comarca',
                    'type'            => 'text',
                    'show_on_cb'      => 'cmb2_hide_if_no_cats', // function should return a bool value
            ) );*/
            
            $cmb->add_field( array(
                    'name'            => 'Data da suspensão',
                    'desc'            => __( 'Data da suspensão.', 'cmb2' ),
                    'id'              => $prefix .'data_suspensao',
                    'type'            => 'text_date',
                    'date_format'     => 'd/m/Y',
                    'attributes'      => array('required' => 'required')
            ) );
            
            $cmb->add_field( array(
                    'name'            => 'Emenda',
                    'desc'            => __( 'Data de emenda. (opcional)', 'cmb2' ),
                    'id'              => $prefix . 'emenda',
                    'type'            => 'text_date',
                    'date_format'     => 'd/m/Y',
            ) );

    }


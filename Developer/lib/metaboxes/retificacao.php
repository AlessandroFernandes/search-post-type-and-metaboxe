<?php

    add_action( 'cmb2_admin_init', 'retificacao_metaboxes' );

    function retificacao_metaboxes() {

            $prefix = 'AASP_';
            
            $cmb = new_cmb2_box( array(
                    'id'              => 'retificacao',
                    'title'           => __( 'Retificação', 'cmb2' ),
                    'object_types'    => array( 'suspensao'), // Post type
                    'context'         => 'normal',
                    'priority'        => 'high',
                    'show_names'      => true, // Show field names on the left
                    // 'cmb_styles' => false, // false to disable the CMB stylesheet
                    // 'closed'     => true, // Keep the metabox closed by default
            ) );

            $cmb->add_field( array(
                    'name'            => 'Data da retificacão',
                    'desc'            => __( 'Data da retificação. (opcional)', 'cmb2' ),
                    'id'              => $prefix .'data_retificacao',
                    'type'            => 'text_date',
                    //'date_format'     => 'd/m/Y',
                    //'attributes'      => array('required' => 'AASP_retificacao')
            ) );

            $cmb->add_field( array(
                    'name' => 'Retificação',
                    'desc' => 'Texto da retificação (opcional)',
                    //'default' => 'standard value (optional)',
                    'id' => $prefix . 'retificacao',
                    'type' => 'textarea_small'
            ) );
    }

  
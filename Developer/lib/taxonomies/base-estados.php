<?php

        add_action('tribunais_add_form_fields', 'add_suspensao_fields', 10, 2);

        function add_suspensao_fields($taxonomy){
                ?>      
                <label for="uf">UF</label>
                <input type="text" name="uf" id="uf"><br><br>
        <?php
        }


        add_action('created_tribunais', 'save_estados_meta', 10, 2);
        
        function save_estados_meta($term_id, $tt_id){
                if(!empty($_POST["uf"])){
                        add_term_meta($term_id, 'uf', $_POST["uf"], true);
                };
        }

        add_action('tribunais_edit_form_fields', 'edit_estados_fields', 10, 2);        
        
        function edit_estados_fields($term, $taxonomy){
                global $uf;
                $uf = get_term_meta($term->term_id, 'uf', true);
        ?>
                <tr>
                        <th>UF</th>
                        <td>
                                <div>
                                        <input type='text' name="uf" id="uf" value="<?= $uf ?>">
                                </div>
                        </td>
                <tr>
        <?php
         }

        add_action('edited_tribunais', 'update_tribunais_meta', 10, 2);

        function update_tribunais_meta($term_id, $tt_id){
                if(!empty($_POST['uf'])){
                        update_term_meta($term_id, 'uf', $_POST['uf']);
                }
        }

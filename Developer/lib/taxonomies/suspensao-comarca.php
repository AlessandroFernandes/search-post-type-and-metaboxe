<?php

    add_action('init', 'suspensao_taxonomy',0);

    function suspensao_taxonomy(){

            $labels = array(
                    'name'                => _x('Comarcas', 'taxonomy general name'),
                    'singular name'       => _x('Comarca', 'taxonomy singular name'),
                    'search_items'        => __('Buscar suspensão'),
                    'all_items'           => __('Todas as comarcas / Tribunais'),
                    //'parent_item'         => __('Comarca / Tribunal'),
                   // 'parent_item_colon'   => __('Parent Tópicos'),
                    'edit__item'          => __('Editar comarca'),
                    'update_item'         => __('Atualizar comarca'),
                    'add_new_item'        => __('Adicionar comarca'),
                    'new_item_name'       => __('Novo tópico'),
                    'menu_name'           => __('Adicionar tribunal'),
                    'parent_item'                => null,
                    'parent_item_colon'          => null,
            );
            register_taxonomy('tribunais', array('suspensao'), array(
                    'hierarchical'        => true,
                    'labels'              => $labels,
                    'label'               => 'Comarca',
                    'show_ui'             => true,
                    'show_in_menu'        => true,
                    'show_in_nav_menus'   => true,
                    'show_admin_column'   => false,
                    'query_var'           => true,
                    'rewrite'             => array('slug' => 'topicos'),
                    'public'              => true,
                    //'update_count_callback' => '_update_post_term_count',

            ));
    };


   // require_once 'base-estados.php';
   // require_once 'base-cidade.php';


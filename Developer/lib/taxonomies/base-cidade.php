<?php

        add_action('tribunais_add_form_fields', 'add_cidade_fields', 10, 2);

        function add_cidade_fields($taxonomy){
                ?>      
                <label for="cidade">Cidade</label>
                <input type="text" name="cidade" id="cidade"><br><br>
        <?php
        }

        add_action('created_tribunais', 'save_cidade_meta', 10, 2);

        function save_cidade_meta($term_id, $tt_id){
                if(!empty($_POST["cidade"])){
                        add_term_meta($term_id, 'cidade', $_POST["cidade"], true);
                };
        }

        add_action('tribunais_edit_form_fields', 'edit_cidade_fields', 10, 2);        

        function edit_cidade_fields($term, $taxonomy){
                global $cidade;
                $cidade = get_term_meta($term->term_id, 'cidade', true);
        ?>
                <tr>
                        <th>Cidade</th>
                        <td>
                                <div>
                                        <input type='text' name="cidade" id="cidade "value="<?= $cidade ?>">
                                </div>
                        </td>
                <tr>
        <?php
        }

        add_action('edited_tribunais', 'update_cidade_meta', 10, 2);

        function update_cidade_meta($term_id, $tt_id){
                if(!empty($_POST['cidade'])){
                        update_term_meta($term_id, 'cidade', $_POST['cidade']);
                }
        }

<?php

    add_action('init', 'datasuspensao_taxonomy');

    function datasuspensao_taxonomy(){

        $labels = array(
                  'name'            => _x('Data de suspensão','taxonomy general name'),
                  'singular_name'   => _x('Data de suspensão','taxonomy singular name'),
                  'all_items'       => __('Buscar data'), 
                  'menu_name'       => __('Adicionar data')           
        );

        register_taxonomy('data_suspensao', 'suspensao', array(
                  'labels'          => $labels,
                  'label'           => "Data de suspensão",
                  'show_ui'         => true,
                  'hierarchical'    => true,
                  'public'          => true,
                  'rewrite'         => array('slug' => 'tópicos')
        ));
    }
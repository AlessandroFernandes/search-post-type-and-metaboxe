<?php 

    add_action('init', 'buscar_suspensao');
  
        function buscar_suspensao(){
                $name = 'Suspensões';
                $name_singular = 'Suspensão';
                $description = 'Sistema de busca de suspensão';
                
                $labels = array(
                        'name'           => $name,
                        'singular_name'  => $name_singular,
                        'menu_name'      => $name,
                        'add_new_item'   => 'Nova suspensão',
                        'add_new'        => 'Nova suspensão',
                        'edit_item'      => 'Editar suspensão',
                        'search_items'   => 'Buscar suspensão',
                        'all_items'      => 'Todas as suspensões'            
                );

                $supports = array(
                        'title',
                        'editor',
                        'excerpt',
                        'thumbnail',
                        'comments',
                        'revisions',
                        'custom-fields',
                        //  'trackbacks',
                        //  'page-attributes',
                        //  'post-formats'
                );
        

                $args = array(
                        'label'               => $name,
                        'labels'              => $labels,
                        'public'              => true,
                        'description'         => $description,
                        'supports'            => $supports,
                        'menu_icon'           => 'dashicons-code-standards',
                        'taxonomies'          => array('tribunais'),
                        'hierarchical'        => false,
                        'public'              => true,
                        'show_ui'             => true,
                        'show_in_menu'        => true,
                        'show_in_nav_menus'   => true,
                        'show_in_admin_bar'   => true,
                        //'show_in_rest'        => false,
                        'menu_position'       => 2,
                        'can_export'          => true,
                        'has_archive'         => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'capability_type'     => 'post',
                        'rewrite'             => array( 'slug' => 'events' ),
                );

                register_post_type('suspensao', $args);
        };


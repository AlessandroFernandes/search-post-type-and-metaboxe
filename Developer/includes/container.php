 <?php 
    $seletor_tribunal = array_key_exists('taxonomy', $_GET);
    $seletor_dia = array_key_exists('taxonomy_data', $_GET);
    global $query_string;

?>

<h5 class="h5 mb-3">Filtrado por:</h5>
<p class="h6 mb-3"><?= $_GET['taxonomy'] ? $_GET['taxonomy'] : $_GET['taxonomy_data'] ?></p>
<div class="card">
    <div class="card-body">    
        <?php 
        if($seletor_tribunal && !$seletor_dia){
            $seletor = array('tribunais','taxonomy') ;
        }else if(!$seletor_tribunal && $seletor_dia){
            $seletor = array('data_suspensao','taxonomy_data');
        }else{
            $seletor = null;
        };

        if($seletor){
            $taxonomy_args = array(
                    array(
                        'taxonomy' => $seletor[0],
                        'field'    => 'slug',
                        'terms'    => $_GET[$seletor[1]]
                    ));
            $args = array(
                        'post_type' => 'suspensao',
                        'tax_query' => $taxonomy_args
            );
            $loop = new WP_Query($args);
            ?>
            <?php 
            require_once __DIR__ .'/container-resultado-query.php';
            wp_reset_postdata();
            

        }else{

            global $query_string;
            wp_parse_str( $query_string, $args );
            $loop = new WP_Query($args );
            require_once __DIR__ .'/container-resultado-query.php';

        }
            ?>
    </div>
</div>
                                                                                            
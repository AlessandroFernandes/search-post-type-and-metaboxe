<div class="card grey lighten-3">
    <div class="card-header greenAasp">
                Ou buscar por região
    </div>
    <div class="card-body">
        <select class="browser-default custom-select">
            <option value="1" selected>São Paulo</option>
        </select>
        <br>

        <?php $taxonomies = get_terms('tribunais') ?>
        <br>
        <form class="" action="<?= bloginfo('url'); ?>/" method="get">
            <div id="filtro" class="">
                <select  class="browser-default custom-select" name="taxonomy" onchange="this.form.submit()">
                    <option  value="" disabled selected>Comarca / Tribunal</option>
                        <?php foreach($taxonomies as $taxonomia) :?>
                            <option value="<?= $taxonomia->slug; ?>"><?= $taxonomia->name; ?></option>
                        <?php endforeach; ?>
                </select>
            </div>
        </form>
        
        <?php $taxonomies = get_terms('data_suspensao') ?>
        <br>
        <form class="" action="<?= bloginfo('url'); ?>/" method="get">
            <div id="filtro" class="">
                <select  class="browser-default custom-select" name="taxonomy_data" onchange="this.form.submit()">
                    <option  value="" disabled selected>Data de suspensão</option>
                        <?php foreach($taxonomies as $taxonomia) :?>
                            <option value="<?= $taxonomia->slug; ?>"><?= $taxonomia->name; ?></option>
                        <?php endforeach; ?>
                </select>
            </div>
        </form>
    </div>
</div>
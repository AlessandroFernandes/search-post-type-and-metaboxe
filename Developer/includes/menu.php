
    <div class="card">
        <div class="card grey grey lighten-3">
            <div class="card-body">
                <div class="card-text">
                     <form>
                        <div class="md-form greenAasp">
                            <i class="fas fa-search  prefix"></i>
                            <input mdbActive type="search" class="form-control" id="s" name="s" Placeholder="Buscar" value="<?php the_search_query() ?>">
                            <label class="active" for="form9">Buscar Suspensão</label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php require_once __DIR__ . '/buscar-suspensao.php' ?>
    </div>

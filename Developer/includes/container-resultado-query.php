<?php
            if($loop->have_posts()): 
                        while($loop->have_posts()):  
                                $loop->the_post(); ?>
                                        <?php if(get_post_meta( get_the_ID(), 'AASP_retificacao', true )) : ?>
                                                <div class="alert-danger">
                                                    <h2 class="text-center h2">Atenção<h2>
                                                        <h5 class="text-center">Houve uma retificação desta suspensão no dia <b><?= get_post_meta( get_the_ID(), 'AASP_data_retificacao', true); ?></b></h5>
                                                        <h6 class="text-justify mr-4 ml-4 pb-2 pt-2"><b>Motivo: </b><?= get_post_meta( get_the_ID(), 'AASP_retificacao', true) ?></h6>
                                                </div>
                                        <?php endif; ?>
                                        
                                        <h5 class="h5 greenAasp"><?php the_title() ?></h5>
                                        <hr>    
                                            <p class="note note-success mb-10"><b>Expediente suspenso: <?= get_post_meta( get_the_ID(), 'AASP_data_suspensao', true ); ?></b></p>
                                            <?php the_content() ?>
                                        <div class="text-right mb-3">
                                            <small><i>Data de publicação: <?= the_date() ?></i></small>
                                        </div>
                                        <br>
            <?php    
                            endwhile; 
                endif;
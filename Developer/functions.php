<?php 

        if ( file_exists( __DIR__ . '/inc/cmb2/init.php' ) ) {
                require_once  __DIR__ . '/inc/cmb2/init.php';
        } 

        wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/main.css', false, 1.0, 'all');

        require_once __DIR__ .'/lib/custom-posts/suspensao.php';
        require_once __DIR__ .'/lib/metaboxes/suspensao.php';
        require_once __DIR__ .'/lib/metaboxes/retificacao.php';
        require_once __DIR__ .'/lib/taxonomies/suspensao-comarca.php';
        require_once __DIR__ .'/lib/taxonomies/data-suspensao.php';
        // require_once __DIR__ .'/lib/taxonomies/estados.php';
        // require_once __DIR__ .'/lib/taxonomies/cidade.php';

        function complemento_estilos(){
                wp_enqueue_style('fontawesome', "https://use.fontawesome.com/releases/v5.8.2/css/all.css");
                wp_enqueue_style('bootstrap_css',"https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css");
                wp_enqueue_style('mbd_css',"https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.3/css/mdb.min.css");
                wp_enqueue_style('main', get_stylesheet_directory_uri(). '/assets/css/main.css');
                wp_enqueue_script('jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js");
                wp_enqueue_script('popper', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js");
                wp_enqueue_script('bootstrap_js', "https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js");
                wp_enqueue_script('mdb_js', "https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.3/js/mdb.min.js");
        };

        add_action('init', "complemento_estilos");
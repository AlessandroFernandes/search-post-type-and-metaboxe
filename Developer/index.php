<?php include_once __DIR__ . '/includes/header.php'; ?>
    
    <div class="jumbotron">
        <h1> Development Test </h1>
    </div>
 
    <div class="row">
        <div class="container">        
            <hr>
                Início / teste_teste / Suspensão
            <hr>

            <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <?php include_once __DIR__ .'/includes/menu.php' ?>
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8">
                    <?php include_once __DIR__ .'/includes/container.php' ?>
                </div>   
            </div>
        </div>
    </div>
    
<?php include_once __DIR__ .'/includes/footer.php'; ?>